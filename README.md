# sfml-gol
Conway's Game of Life with SFML

### Controls:

* `Q`, `[ESC]` quits
* `[SPACE]` advances one generation
* `P` pause/unpause
* `R` randomly fill cell grid
* `C` clear grid
* `L` load file
* `S` save file
* `T` print state to stdout
* Left click creates a cell at pointer, right click removes the cell.

Save file is `state.gol`

### Requires:
* make
* sfml-devel
