/** @file life.cpp
    Conway's game of life in SFML
 
    Controls:
    Q/Esc - Quit
    SPACE - Step
    P - Pause/play simulation (at FRAMERATE)
    T - Print simulation state to stdout
    R - Fill grid randomly
    S - Save state to state.gol file
    L - Load file to grid
    C - Clear state (kill all cells)
 
    @version 1.0 11/28/17
    @author Galen Asphaug
*/

#include <iostream>
#include <fstream>
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "CellGrid.hpp"

// Settings
const unsigned WIDTH  = 100; // Number of cells
const unsigned HEIGHT = 100;
const unsigned CELL_SIZE = 10; // Size of cell in pixels
const unsigned FPS = 20;

const std::string FILENAME = "state.gol";
const float RANDOM_PERCENT = 0.15f; // How much of the board should be covered
const sf::Color LIVE_COLOR = sf::Color::Black;
const sf::Color DEAD_COLOR = sf::Color::White;

/**
   Loads a file as the grid
   @param &grid the grid to save
*/
void load(CellGrid &grid);

/**
   Saves the state to a file
   @param &grid the grid to save
*/
void save(CellGrid &grid);

int main() {
  // Use these shapes to draw cells
  sf::RectangleShape cellShape(sf::Vector2f(CELL_SIZE, CELL_SIZE));
    
  // I originally used a vector of rectangles here, but using a single shape and using it like
  // a stamp to draw its shape to the window and move it to a new location seemed more efficient
  cellShape.setFillColor(LIVE_COLOR);
    
  // Only draw if changed
  bool redraw = true;
  // Run animation when 'P' pressed
  bool pause = true;
    
  // Create the main window
  sf::RenderWindow window(sf::VideoMode(WIDTH * CELL_SIZE, HEIGHT * CELL_SIZE),
			  "Game of Life", sf::Style::Close); //Make window size static
  window.clear(DEAD_COLOR);
    
  //window.setVerticalSyncEnabled(true);
  window.setFramerateLimit(FPS);

  // Create the CelllGrid
  CellGrid life(WIDTH, HEIGHT);
    
  // Start with an acorn in the middle
  life.acorn(WIDTH / 2, HEIGHT / 2);
    
  // Start the window loop
  while (window.isOpen()) {
    // Process events
    sf::Event event;
    while (window.pollEvent(event)) {
      // Close window: exit
      if (event.type == sf::Event::Closed) {
	window.close();
      }
      // Pause when lost focus
      if (event.type == sf::Event::LostFocus) {
	pause = true;
      }
      // Process key presses
      if (event.type == sf::Event::KeyPressed) {
	switch (event.key.code) {
	// Escape/Q pressed: exit
	case sf::Keyboard::Escape:
	case sf::Keyboard::Q:
	  window.close();
	  break;

	// Space: evolve
	case sf::Keyboard::Space:
	  pause = true;
	  life.evolve(); // Calculate next life
	  redraw = true; // Mark as changed
	  break;
                        
	case sf::Keyboard::P:
	  pause = !pause;
	  if (pause)
	    redraw = false;
	  break;
                        
	case sf::Keyboard::C:
	  life.exterminate();
	  redraw = true;
	  break;
                    
	case sf::Keyboard::R:
	  life.random(RANDOM_PERCENT);
	  redraw = true;
	  break;
                        
	case sf::Keyboard::T:
	  life.print();
	  break;
                        
	case sf::Keyboard::L:
	  load(life);
	  redraw = true;
	  break;
                        
	case sf::Keyboard::S:
	  save(life);
	  break;
                        
	default:
	  std::cout << "Key not handled: " << event.key.code << std::endl;
	  break;
	}
      } //end key check
            
      // Process mouse clicks
      if (event.type == event.MouseButtonPressed) {
	sf::Vector2i mousePosition = sf::Mouse::getPosition(window);
	//Left mouse
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left)) {
	  //Generate cells on pointer
	  life.revive((mousePosition.x / CELL_SIZE),(mousePosition.y / CELL_SIZE));
	  redraw = true;
	} else if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right)) {
	  //Kill cells on pointer
	  life.exterminate((mousePosition.x / CELL_SIZE),(mousePosition.y / CELL_SIZE));
	  redraw = true;
	}
      }
    } //end event loop

    if (!pause) {
      redraw = true;
      life.evolve();
    }
        
    // Only update display if dish changed
    if (redraw) {
      // Clear screen
      window.clear(DEAD_COLOR);
            
      // For each value, draw a live cell in its location if it is alive
      for (unsigned y = 0; y < HEIGHT; ++y) {
	for (unsigned x = 0; x < WIDTH; ++x) {
	  if (life.isAlive(x, y)) {
	    cellShape.setPosition(x * CELL_SIZE, y * CELL_SIZE);
	    window.draw(cellShape);
	  }
	}
      }

      // Update the window
      window.display();
      // Stop from drawing again unless something changes
      redraw = false;
    }
    // Keep CPU usage low while paused
    sf::sleep(sf::milliseconds(1));
  }
  //window closed
  return EXIT_SUCCESS;
}

void load(CellGrid &grid) {
  bool corrupt = false;
  std::string state; //grid
  std::ifstream fin(FILENAME.c_str());
  
  if (fin.good()) {
    // Read one line at a time
    do {
      std::string currentLine;
      getline(fin, currentLine);
      if (currentLine.length() == WIDTH) {
	state.append(currentLine);
      } else {
	// Empty lines are OK
	if (currentLine.length() != 0) {
	  std::cout << "Improper line length.\n";
	  corrupt = true;
	}
      }
    } while (fin.good() && !fin.eof());
    
    // Make sure data is just ones and zeroes
    for (char c : state) {
      if (c != '0' && c != '1') {
	std::cout << "Foreign character.";
	corrupt = true;
      }
    }
    
    // Only load if not corrupt
    if (!corrupt) {
      grid.load(state);
    } else {
      std::cout << "File is corrupt.\n";
    }
  } else {
    std::cout << "Error opening file.\n";
  }
  fin.close();
}

void save(CellGrid &grid) {
  // If only loading were this easy...
  std::ofstream fout(FILENAME.c_str());
  if (fout.good()) {
    grid.save(fout);
  } else {
    std::cout << "Couldn't save file.\n";
  }
  fout.close();
}
