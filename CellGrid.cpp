/** @file CellGrid.cpp
    @author Galen Asphaug
    @version 1.0 12/1/17
    A 2d square grid of boolean values for cellular automata
*/

#include "CellGrid.hpp"
#include <iostream>

CellGrid::CellGrid() {
  width = DEFAULT_WIDTH;
  height = DEFAULT_WIDTH;
  cells.resize(height, std::vector<bool> (width, 0));
}

CellGrid::CellGrid(unsigned w, unsigned h) {
  width = w;
  height = h;
  cells.resize(height, std::vector<bool> (width, 0));
}

void CellGrid::evolve() {
  //Stores number of neighbors for each cell
  //std::vector< std::vector<bool> > neighborGrid;
  //neighborGrid.resize(height, std::vector<bool> (width, 0));
  char neighborGrid[height][width];
  for (unsigned y = 0; y < cells.size(); y++) {
    for (unsigned x = 0; x < cells[y].size(); x++) {
      neighborGrid[y][x] = getNeighbors(x, y);
    }
  }
  // Now that we have neighbors backed up, we can change cells values.
    
  for (unsigned y = 0; y < cells.size(); y++) {
    for (unsigned x = 0; x < cells[y].size(); x++) {
      unsigned neighbors = neighborGrid[y][x];
            
      // Underpopulation / overpopulation
      if (isAlive(x, y) && (neighbors <= UNDERPOPULATED || neighbors >= OVERPOPULATED)) {
	exterminate(x, y);
	// Resurrection
      } else if (!isAlive(x, y) && neighbors == RESURRECTION) {
	revive(x, y);
      }
    }
  }
}

unsigned CellGrid::getNeighbors(unsigned x, unsigned y) const {
  unsigned char neighbors = 0;
  for (int yOffset = -1; yOffset <= 1; yOffset++) { // scan height
    for (int xOffset = -1; xOffset <= 1; xOffset++) { // scan width
      // If neighbor is in the grid and not itself
      if (inBounds(x + xOffset, y + yOffset) && !(yOffset == 0 && xOffset == 0)) {
	if (isAlive((x + xOffset),(y + yOffset))) {
	  neighbors++;
	}
      }
    }
  }
  return neighbors;
}

bool CellGrid::inBounds(unsigned x, unsigned y) const {
  // Unsigned can't be negative so that makes this check easier
  return ((y < height) && (x < width));
}


void CellGrid::exterminate() {
  for (unsigned y = 0; y < cells.size(); y++) {
    for (unsigned x = 0; x < cells[y].size(); x++) {
      cells[y][x] = DEAD;
      // Don't call exterminate(x,y) because it would keep checking
      // if valid cells were in bounds
    }
  }
}

void CellGrid::exterminate(unsigned x, unsigned y) {
  if (inBounds(x, y))
    cells[y][x] = DEAD;
}

void CellGrid::revive(unsigned x, unsigned y) {
  if (inBounds(x, y))
    cells[y][x] = ALIVE;
}

void CellGrid::random(float percent) {
  srand((unsigned)time(0));
  for (unsigned y = 0; y < cells.size(); y++) {
    for (unsigned x = 0; x < cells[y].size(); x++) {
      if (((double)rand() / (double)RAND_MAX) < percent) {
	// Revives cell
	cells[y][x] = ALIVE;
      }
    }
  }
}

void CellGrid::print(std::ostream &out) const {
  for (unsigned y = 0; y < cells.size(); y++) {
    for (unsigned x = 0; x < cells[y].size(); x++) {
      if (isAlive(x, y)) {
	out << 1;
      } else {
	out << 0;
      }
    }
    out << std::endl;
  }
}

void CellGrid::print() const {
  print(std::cout);
}

void CellGrid::penta_decathon(unsigned x, unsigned y) {
  //fills a line of 10 cells. causes a cool repeating pattern
  //exterminate();
  const int PENTA_DECATHON_LENGTH = 10;
  for (int i = -(PENTA_DECATHON_LENGTH / 2); i < PENTA_DECATHON_LENGTH / 2; i++) {
    if (inBounds(x + i, y)) {
      cells[y][x + i] = ALIVE;
    }
  }
}

void CellGrid::acorn(unsigned x, unsigned y) {
  const int A1 = 1, A2 = 2, A3 = 3, A4 =4, A5 = 5, A6 = 6;
  // Don't draw if it would put the shape off the grid
  if (!inBounds(x, y) || !inBounds(x + A6, y - A2)) {
    return;
  }
  //exterminate();
  cells[y - A2][x + A1] = ALIVE;
  cells[y - A1][x + A3] = ALIVE;
  cells[y][x] = ALIVE;
  cells[y][x + A1] = ALIVE;
  cells[y][x + A4] = ALIVE;
  cells[y][x + A5] = ALIVE;
  cells[y][x + A6] = ALIVE;
}

bool CellGrid::isAlive(unsigned x, unsigned y) const {
  return cells[y][x];
}

void CellGrid::load(std::string state) {
  for (unsigned y = 0; y < height; y++) {
    for (unsigned x = 0; x < height; x++) {
      if (state[x + (y * width)] == '1') {
	revive(x, y);
      } else if (state[x + (y * width)] == '0') {
	exterminate(x, y);
      }
    }
  }
}

void CellGrid::save(std::ofstream &file) const {
  print(file);
}
