# makefile for sfml-gol

EXE = sfml-gol
CC = g++
CFLAGS = -Wall -Wpedantic -std=c++11
SFML_LIBS = -lsfml-system -lsfml-graphics -lsfml-window

$(EXE): Life.cpp CellGrid.o CellGrid.cpp CellGrid.hpp
	$(CC) $(CFLAGS) Life.cpp CellGrid.o $(SFML_LIBS) -o $(EXE)

CellGrid.o: CellGrid.hpp CellGrid.cpp
	$(CC) $(CFLAGS) -c CellGrid.cpp -o CellGrid.o

run: $(EXE)
	./$(EXE)

clean:
	rm -rf $(EXE) *.o *~ .*~
