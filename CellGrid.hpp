/** @file CellGrid.hpp
    @author Galen Asphaug
    @version 1.0 12/1/17
    A 2d square grid of boolean values for cellular automata
*/

#ifndef CellGrid_hpp
#define CellGrid_hpp

#include <SFML/Graphics.hpp> // sf::Color
#include <fstream>

class CellGrid{
private:
  // 2D vector of bools. (X = 0 and Y = 0 at top left)
  std::vector< std::vector<bool> > cells; // ([y][x])
    
  // To make code more readable
  const static bool ALIVE = 1, DEAD = 0;
    
  // Rules
  const static unsigned UNDERPOPULATED = /*<=*/1,
    OVERPOPULATED = /*>=*/4,
    RESURRECTION = 3;
    
  const static unsigned DEFAULT_WIDTH = 10, DEFAULT_HEIGHT = 10;
    
  // Settings
  unsigned width, height;

public:
  /**
     Create a new cell grid with default width and height
  */
  CellGrid();
    
  /**
     Create a new cell grid with width and height
     @param w width
     @param h height
  */
  CellGrid(unsigned w, unsigned h);
    
  /**
     Evolve the current grid one generation
  */
  void evolve();
    
  /**
     Calculate the number of neighbors that a cell has from its coordinates
     Coordinates start at the top left of the grid
     Returned value does not include the current cell
     @param x the x value of the cells location
     @param y the y value of the cells location
     @return the number of neighbors the cell has (not including itself)
  */
  unsigned getNeighbors(unsigned x, unsigned y) const;
    
  /**
     Checks if a cell is within the bounds of the dish
     @return true if cell is within the bounds of the grid
  */
  bool inBounds(unsigned x, unsigned y) const;
    
  /**
     Kill all cells on the grid
  */
  void exterminate();
    
  /**
     Kills the cell at [y][x]
     @param x the x value of the cell
     @param y the y value of the cell
  */
  void exterminate(unsigned x, unsigned y);
    
  /**
     Revives a cell at [y][x]
     @param x the x value of the cell
     @param y the y value of the cell
  */
  void revive(unsigned x, unsigned y);
    
  /**
     Fill the grid randomly with live cells
     @param percent the percent of cells to fill
  */
  void random(float percent);
    
  /**
     Print dish to an ostream as a square grid of 0s and 1s
     @param out the ostream to write to
  */
  void print(std::ostream &out) const;
    
  /**
     Print dish to stdout as a square grid of 0s and 1s
  */
  void print() const;
    
  /**
     Checks if a cell is live or dead
     
     @param x the x value of the cell
     @param y the y value of the cell
     @return true if the cell is alive
  */
  bool isAlive(unsigned x, unsigned y) const;
    
  /**
     Clear all points and load a map from a file
  */
  void load(std::string state);
    
  /**
     Save all points to a file
  */
  void save(std::ofstream &file) const;
    
  /**
     The following two functions load some cool preset configurations
  */
    
  /**
     Draw the "Penta-Decathon" to the board:
     
     @param x the x coordinate
     @param y the y coordinate
  */
  void penta_decathon(unsigned x, unsigned y);
    
  /**
     Draw the "Acorn" to the board:
     
     @param x the x coordinate
     @param y the y coordinate
  */
  void acorn(unsigned x, unsigned y);
};

#endif /* CellGrid_hpp */
